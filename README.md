Welcome to course materials on Qt framework.

Contents:
--------

1. Meta-object subsystem and its features.
2. Signals and slots (observer pattern).
3. Simplistic manually laid out GUI forms.

