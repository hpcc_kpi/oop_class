#include "sender.h"
#include "receiver.h"
#include "proxy.h"

void freeSlot(int x) {
  std::cout << "free slot called for " << x << std::endl;
}

int main() {
  Sender S;
  Receiver R;
  Proxy P;

  // Use 'connect' method defined in QObject to tie signals and slots.
  // SIGNAL and SLOT macros are used to name corresponding signals and slots in
  // Qt4.  Signals and slots are connected in the run time using meta-object
  // system, if connect fails due to some error, the program continues without
  // this connection and a debug message is printed.  No compile-time checking
  // of signal and slot signatures is performed.
  QObject::connect(&S, SIGNAL(fire(int)),
                   &R, SLOT(processInt(int)));

  // Keep in mind that Qt meta-object system matches signals and slots as text
  // strings, so implicit arguments conversions are not possible.
  QObject::connect(&S, SIGNAL(fire(int)),
                   &R, SLOT(processMyInt(MyInt)));

  // Slots can have less arguments than signals do, extra arguments will be
  // ignored.  The inverse is not allowed though.
  QObject::connect(&S, SIGNAL(fire(int)),
                   &R, SLOT(processEmpty()));

  // Signals can be connected to other signals in case you need to pass a
  // signal through a proxy-like object.
  QObject::connect(&S, SIGNAL(fire(int)),
                   &P, SIGNAL(fireProxy(int)));
  QObject::connect(&P, SIGNAL(fireProxy(int)),
                   &R, SLOT(processInt(int)));

  S.start();

  // Slots can be called just like normal functions.  Signals can't.
  R.processInt(7); 

  // Qt5 introduces new syntax for 'connect' with compile-time checking.
  QObject::connect(&S, &S::fire,
                   &R, &R::processInt);
  
  // It can also be used with free functions...
  QObject::connect(&S, &S::fire, &freeSlot);

  // ... and lambda expressions.
  QObject::connect(&S, &S::fire, 
                   [](int x) 
                   { 
                     std::cout << "inside lambda: " << x << std::endl; 
                   } -> void);

  return 0;
}
