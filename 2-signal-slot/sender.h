#ifndef SENDER_H
#define SENDER_H

// QtCore
#include <QtCore>

// QObject is the base class for most Qt entities.  One should inherit it if Qt
// meta-object system is used with some classes.
class Sender : public QObject {
  // Q_OBJECT macro enbales meta-information generation via moc (Qt meta-object
  // compiler).
  Q_OBJECT

// Signals are special functions that can be used to notify other objects about
// events in this object.  Signals must not have explicit bodies.  Signals are
// delcared protected by default. Only the clases that have meta-information
// can declare signals and define slots.  Signals and slots are subject to
// inheritence.
signals:
  void fire(int);

public:
  void start() {
    // The 'emit' keyword invokes the signal.  The slots, connected to the
    // signal being emitted, will be called in the same order they were
    // connected.
    emit fire(3);
  }
};

#endif // SENDER_H

