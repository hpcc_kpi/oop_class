#ifndef PROXY_H
#define PROXY_H

#include <QtCore>

class Proxy : public QObject {
  Q_OBJECT

signals:
  void fireProxy(int);
};

#endif // PROXY_H
