#ifndef RECEIVER_H
#define RECEIVER_H

#include <QtCore>
#include <iostream>

// Just an alias for int.
typedef int MyInt;

class Receiver : public QObject {
  Q_OBJECT

// Slots are special functions that are invoked when some signal is triggered.
// Qt meta-object subsystem handles these call via qt_static_metacall function
// in moc-generated code.  Slots can accept arguments if they match the
// arguments the corresponding signal.
public slots:
  void processInt(int x) {
    std::cout << "accepted: " << x << std::endl;
  }

  void processMyInt(MyInt x) {
    std::cout << "accepted: " << x << std::endl;
  }

  void processEmpty() {
    std::cout << "nothing accepted" << std::endl;
  }
};

#endif // RECEIVER_H
