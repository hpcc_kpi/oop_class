#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore>
#include <QtGui>

// MainWindow is, quite surprisingly, the application's main window.  It should
// be created only once per application.  It allows adding menus, toolbars,
// status bars, provides tooltip facility, etc.
class MainWindow : public QMainWindow {
  Q_OBJECT

  QLabel *login;

public:
  // All the widgets accept pointer to parent as a constructor argument.  It is
  // useful to ensure their drawing.
  explicit MainWindow(QWidget *parent = 0);

public slots:
  void buttonPressed() {
    login->setText("Ok");
  }
};

#endif // MAINWINDOW_H
