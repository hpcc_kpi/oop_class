#include <QtCore>
#include <QtGui>

#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent) {
  
  login = new QLabel("Login: ");
  QLabel *passwd = new QLabel("Passwd: ");

  QTextEdit *loginInput = new QTextEdit("login");
  QTextEdit *passwdInput = new QTextEdit("passwd");

  // Qt supports a subset of HTML within displayable strings in widgets.
  QPushButton *button = new QPushButton("<b>Log me in!</b>");

  // One can set widgets bounding box directly by passing top left corner
  // coordinates, width and height.
  // Qt coordinate system starts in the top left cornder of the window.
  login->setGeometry(50, 50, 100, 50);
  loginInput->setGeometry(150, 50, 100, 50);
  passwd->setGeometry(50, 100, 100, 50);
  passwdInput->setGeometry(150, 100, 100, 50);
  button->setGeometry(50, 150, 200, 50);
  // Setting up geometry manually is discouraged though.  One would prefer
  // using a layout manager which allows to make resizable forms.

  // When a widget is drawn, all its child widgets are also drawn.
  login->setParent(this);
  loginInput->setParent(this);
  passwd->setParent(this);
  passwdInput->setParent(this);
  button->setParent(this);

  // Interaction between GUI elements and code is performed via signal/slot
  // mechanism.  Most widgets provide numerous signals to get information about
  // user activity and several slots to react to changes in other elements.
  connect(button, SIGNAL(clicked()),
          this, SLOT(buttonPressed()));

  // Since all the widgets have MainWindow as parent, they will be destroyed
  // automatically after MainWindow destruction even though they have been
  // allocated using 'new'.
}
