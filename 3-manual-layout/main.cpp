#include <QtCore>

#include "mainwindow.h"

int main(int argc, char **argv) {
  // QApplication is a special class to deal with your program as an entity.
  // It ensures events from operating system are passed to your code, manages
  // windows, styles, parses command line arguments etc.
  // Only one application object should be constructed!
  // One can refer to the single application object via global qApp pointer.
  // Most its methods are static.
  // An application object should be initialized before any GUI class
  // instances.
  QApplication app(argc, argv);
  MainWindow mw;
  mw.showMaximized();
  
  // Start event processing loop.
  return app.exec();
}
