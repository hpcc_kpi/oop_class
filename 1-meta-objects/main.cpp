#include <QtCore>
#include "class.h"
#include "container.h"

int main() {
  Class *x = new Class(1),
        *y = new Class(2);

  // Each instance of the class inheriting QObject may have parent object.
  // Actually the parent-child relation is stored in QObject as the list of
  // child objects.  This relation allows to build a valid tree of objects.
  y->setParent(x);

  // One can use property accessors as normal methods...
  x->setProp(42);

  // ... or request a property by its textual name.  Qt will call the actual
  // accessor declared in Q_PROPERTY macro and wrap the result type in
  // QVariant.
  // Please be aware that meta-object system is based on string comparisons!
  QVariant prop = x->property("prop");
  std::cout << "prop: " << prop.toInt() << std::endl;

  // If no such property declared, a zeroed QVariant will be returned.
  QVariant another = x->property("bar");
  std::cout << "bar: " << another.toInt() << std::endl;

  // One can set properties using only their names providing a QVariant
  // instance.  'setProperty' returns true if such property had been declared
  // via macro and its value was updated in this call,
  if (x->setProperty("prop", QVariant(43)))
    std::cout << "prop had been declared and was updated" << std::endl;

  // but it returns false if the property can't be updated (doesn't provide
  // WRITE method or is declared CONSTANT).
  if (x->setProperty("another", QVariant(44)))
    std::cout << "another had been declared and was updated" << std::endl;

  // One can set dynamic properties, i.e. the properties that were not declared
  // in the original class.  Dynamic properties are stored in QObject instance
  // and are accessible only by name.  Dynamic properties are stored as
  // QVariant instances.  If the dynamic property was introduced by this call,
  // 'setProperty' returns false.
  // The return type of 'setProperty' cannot be used to determine successful
  // property update or insertion!
  if (x->setProperty("foo", QString("Hello")))
    std::cout << "foo had been declared" << std::endl;

  // Dynamic properties can be read as well.
  QVariant str = x->property("foo");
  std::cout << "foo: " << str.toString().toStdString() << std::endl;

  // To build a QVariant for user-defined class instance, one can use
  // QVariant::fromValue static function template.
  x->setProperty("customType", QVariant::fromValue(Container(77)));
  QVariant var = x->property("customType");
  // Function template 'canConvert' is used to check whether the variant can be
  // converted to the type specified as template argument
  if (var.canConvert<Container>())
    // while 'value' function template is used to cast the variant to any type.
    std::cout << "custom integer wrapper: " << var.value<Container>().value 
              << std::endl;
  else
    std::cout << "can't convert to custom type" << std::endl;

  // Meta-object subsystem creates meta-object information for respective
  // classes.  This information is used to obtain run-time class information...
  const QMetaObject *metaX = x->metaObject();
  std::cout << "class name: " << metaX->className() << std::endl;
  std::cout << "superclass name: " << metaX->superClass()->className() << std::endl;
  std::cout << "method count: " << metaX->methodCount() << std::endl;
  std::cout << "properties: " << std::endl;
  for (int i = metaX->propertyOffset(),
           e = metaX->propertyCount();
      i < e; i++) {
    std::cout << "\t" << metaX->property(i).name() << std::endl;
  }

  // ... or invoke methods, signals and slots.  Note that such calls are slower
  // than normal and no compile-time checking is performed (as usual,
  // meta-object subsystem operates in text-based mode).  If the method with
  // corresponding signature was not found in run-time, invokeMethod returns
  // false and debug message is printed.
  const int newProp = 55;
  QMetaObject::invokeMethod(x, "setProp", QGenericArgument("int", &newProp));
  std::cout << "prop: " << x->prop() << std::endl;

  // Once an instance of the class inheriting QObject gets destroyed, it
  // destroys all its children (children of children, etc).
  // Example: the destructor for y will be called as well.
  delete x;

  // One still have to delete objects that have no parent manually!
}

