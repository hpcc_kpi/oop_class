#include <QtCore>
#include <iostream>

class Class : public QObject {
  // Enable meta-information generation.
  Q_OBJECT

private:
  int value_;
  int prop_;

public:
  explicit Class(int value) : value_(value) {
  }

  virtual ~Class() {
    std::cout << "destroying class with value " << value_ << std::endl;
  }

  // Use Q_INVOKABLE on your methods to allow calling them via meta-objects.
  Q_INVOKABLE void setProp(int prop) {
    prop_ = prop;
  }

  int prop() const {
    return prop_;
  }

  // One can declare properties available to run-time by-name query in classes
  // containing meta-information.  Type Ty is specified in order to guess
  // function return and argument types:
  // READ function is expected to be 'Ty Class::function() const'
  // WRITE function is expected to be 'void Class::function(Ty)', etc.
  Q_PROPERTY(int prop READ prop WRITE setProp);

  // Property name does not necesserily equals existing field name.  One can
  // even declare properties that does not have an associated field.
  // On the other hand, access methods must be defined (Qt won't define
  // property access methods for you).
  Q_PROPERTY(int another READ prop CONSTANT);

  // Q_PROPERTY macro accepts a handful of another options, refer to the
  // documentation to learn them.
};

