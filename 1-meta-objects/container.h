#ifndef CONTAINER_H
#define CONTAINER_H

#include <QtCore>

class Container : public QObject {
  Q_OBJECT

public:
  Container(int v = 0) : value(v)
  {
  }

  Container(const Container &c)
  {
    value = c.value;
  }

  int value;
};

// This macro allows to wrap this type in QVariant safely.
Q_DECLARE_METATYPE(Container)

#endif // CONTAINTER_H
